import express from "express";
import path from "path";
import { UserController } from "./controllers/userController";
import { knex } from "./knex";
import { UserService } from "./services/userService";
import expressSession from "express-session";
import { grantExpress } from "./grantExpress";
import { logger } from "./utils/logger";
import { TemplateController } from "./controllers/templateController";
import { TemplateService } from "./services/templateService";
import multer from "multer";
import { isLoggedInStatic } from "./utils/guard";

const app = express();
// URLENCODED => JS
app.use(express.urlencoded({ extended: true }));

// JSON string => JS
app.use(express.json());
// Create express session middleware
const sessionMiddleware = expressSession({
  secret: "Typescript",
  resave: true,
  saveUninitialized: true,
});
app.use(sessionMiddleware);

// Register for new user
const userService = new UserService(knex);
const userController = new UserController(userService);
app.get("/loadCurrentUser", userController.loadCurrentUser);
app.post("/register", userController.register);

// Google login grant
app.use(grantExpress as express.RequestHandler);
// Login for existing users
app.post("/login", userController.login);
app.get("/login/google", userController.loginGoogle);

// Logout for existing users
app.get("/logout", userController.logout);

// Template
const templateService = new TemplateService(knex);
const templateController = new TemplateController(templateService);

//custom multer(images)
const storage = multer.diskStorage({
  destination: function (req, file, cb) {
    cb(null, path.resolve("./uploads"));
  },
  filename: function (req, file, cb) {
    cb(null, `${file.fieldname}-${Date.now()}.${file.mimetype.split("/")[1]}`);
  },
});

const upload = multer({ storage });
app.post("/cv", upload.single("profile"), templateController.buildCv);
app.get("/getUserInfo", templateController.getInfo);
app.get("/skillSet", templateController.getSkillSet);
app.post("/demo", templateController.userInfoDemo);
app.get("/question", templateController.mockquestion);
app.get("/question/:id", templateController.mockquestion);

app.use("/uploads", express.static(path.resolve("uploads")));
app.use("/images", express.static(path.resolve("images")));
app.use(express.static(path.resolve("public")));
app.use(express.static(path.resolve("secret")));
app.use(isLoggedInStatic, express.static(path.join(__dirname, "private")));

const PORT = 8080;
app.listen(PORT, () => {
  logger.debug(`Listening to http://localhost:${PORT}`);
});
