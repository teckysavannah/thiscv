export const tables = Object.freeze({
  USER: "users",
  USER_CV_DETAILS: "user_cv_details",
  EDUCATION: "education",
  WORK_EXP: "work_exp",
  SKILLSET: "skillset",
  USER_CV_SKILLS: "user_cv_skills",
  MOCK_QUESTTIONS: "mock_questions",
  USERS_MOCK_VIDEO: "users_mock_video",
  OUR_SAMPLE: "our_sample",
  USER_TEMPLATE: "user_template",
});
