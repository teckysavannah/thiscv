import { Request, Response, NextFunction } from "express";

export function isLoggedInStatic(
  req: Request,
  res: Response,
  next: NextFunction
) {
  if (req.session?.["user"]) {
    next();
  } else {
    res.redirect("/login.html");
  }
}