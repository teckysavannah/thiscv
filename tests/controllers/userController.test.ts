import { UserController } from "../../controllers/userController";
import { UserService } from "../../services/userService";
import { Request, Response } from "express"

jest.mock("../../services/userService");

describe("test userController", () => {
    let controller: UserController;
    let service: UserService;
    let req: Request;
    let res: Response;

    beforeEach(() => {
        service = new UserService({} as any);
        controller = new UserController(service);

        // service.login = jest.fn(() =>
        //     Promise.resolve([
        //         {},
        //     ])
        // );

        req = {
            params: {},
            query: {},
            body: {},
            // file: {},
        } as Request;

        res = {
            status: jest.fn(() => res),
            json: jest.fn(),
        } as any as Response;
    });

    it("test login - missing username", async () => {
        req.body.password = "password";

        service.login = jest.fn(() => Promise.reject(new Error("Missing email/password")))

        await controller.login(req, res);
        expect(res.status).toBeCalledWith(400);
        expect(res.json).toBeCalledWith({
            message: "Missing email/password",
        });
        expect(res.json).toBeCalledTimes(1);
    });

    it("test login - missing password", async () => {
        req.body.username = "username";

        service.login = jest.fn(() => Promise.reject(new Error("Missing email/password")))

        await controller.login(req, res);
        expect(res.status).toBeCalledWith(400);
        expect(res.json).toBeCalledWith({
            message: "Missing email/password",
        });
        expect(res.json).toBeCalledTimes(1);
    });
});

