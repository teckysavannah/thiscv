export interface TemplateInfo {
  firstName: string;
  lastName: string;
  address: string;
  description: string;
  phoneNumber: string;
  email: string;
  image: string;
  schoolName: string[];
  programName: string[];
  schoolStartYear: string[];
  schoolEndYear: string[];
  jobTitle: string[];
  companyName: string[];
  companyStartYear: string[];
  companyEndYear: string[];
  skills: string[];
}

// export interface Education {
//   schoolName: string;
//   programName: string;
//   schoolStartYear: string;
//   schoolEndYear: string;
// }
// export interface Work_exp {
//   jobTitle: string;
//   companyName: string;
//   companyStartYear: string;
//   companyEndYear: string;
// }
// [{},{},{}]
// ["","",""]

// {
//   "a":["1","2","3"],
//   "b":[
//     {schoolName: string[];
//     programName: string[];
//     schoolStartYear: string[];
//     schoolEndYear: string[];
//     },
//     {schoolName: string[];
//       programName: string[];
//       schoolStartYear: string[];
//       schoolEndYear: string[];
//     },
//     {schoolName: string[];
//       programName: string[];
//       schoolStartYear: string[];
//       schoolEndYear: string[];
//     }]
// }
