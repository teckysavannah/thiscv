// import { footer } from '../public/js/footer.js'
import { loadCurrentUser } from './currentUser.js'
import { header } from './js/navbar.js'
import { footer } from './js/footer.js'
window.onload = () => {
  header()
  footer()
  loadCurrentUser()
  educationMapInit()
  workMapInit()
}

const checkboxes = document.querySelectorAll('.skill')

const continueToEducation = document.querySelector('#continueToEducation')
continueToEducation.onclick = () => {
  document.querySelector('.profile-container').classList.add('display-none')
  document
    .querySelector('.education-container')
    .classList.remove('display-none')
}
const backToProfile = document.querySelector('#backToProfile')
backToProfile.onclick = () => {
  document.querySelector('.education-container').classList.add('display-none')
  document.querySelector('.profile-container').classList.remove('display-none')
}
const continueToWorkExperience = document.querySelector(
  '#continueToWorkExperience'
)
continueToWorkExperience.onclick = () => {
  document.querySelector('.education-container').classList.add('display-none')
  document
    .querySelector('.workExperience-container')
    .classList.remove('display-none')
}
const continueToSkill = document.querySelector('#continueToSkill')
continueToSkill.onclick = async () => {
  let skillSet = await getSkillSet()
  renderSkillSet(skillSet)
  renderSkillContainerBtns()
  // const res = await fetch('/skillSet')
  // const skillSet = await res.json()
  // const skillContainer = document.querySelector('.skill-container')
  // skillContainer.innerHTML = ''
  // skillSet.forEach((skillInfo) => {
  //   console.log(skillInfo)
  //   skillContainer.innerHTML +=
  //     /*html*/
  //     `
  //       <input type="checkbox" name="skills" class="skill" value="${skillInfo.id}"/>${skillInfo.skill_name}<br/>
  //     `
  // })

  // skillContainer.innerHTML +=
  //   /*html*/
  //   `
  //       <button type="button" id="backToWorkExperience">Back</button>
  //       <button type="submit" id="submittion">Submit</button>
  //     `
  backToWorkExperienceEventListener()
  document
    .querySelector('.workExperience-container')
    .classList.add('display-none')
  document.querySelector('.skill-container').classList.remove('display-none')
}

const backToEducation = document.querySelector('#backToEducation')
backToEducation.onclick = () => {
  document
    .querySelector('.workExperience-container')
    .classList.add('display-none')
  document
    .querySelector('.education-container')
    .classList.remove('display-none')
}

const backToWorkExperienceEventListener = () => {
  document.querySelector('#backToWorkExperience')
  backToWorkExperience.onclick = () => {
    document.querySelector('.skill-container').classList.add('display-none')
    document
      .querySelector('.workExperience-container')
      .classList.remove('display-none')
  }
}

//======================================================================================================
let educationItemId = 0
let educationItemsMap = new Map()
let educationItemValuesMap = new Map()
const educationContainer = document.querySelector('.education-input-container')
const addEducation = document.querySelector('#addEducation')

addEducation.onclick = (e) => {
  educationItemId += 1
  addEducationItem(educationItemId)
  setEducationValue()
  renderEducation()
}

const educationMapInit = () => {
  getEducationValue()
  educationItemsMap.set(`educationItem0`, { html: newEducationItem(0, true) })
}

const getEducationValue = () => {
  const schoolNameInputs = document.querySelectorAll('.schoolName')
  schoolNameInputs.forEach((schoolName) => {
    schoolName.onchange = (e) => {
      educationItemValuesMap.delete(e.target.id)
      educationItemValuesMap.set(e.target.id, e.target.value)
      console.log(e.target.id, ':', educationItemValuesMap.get(e.target.id))
    }
  })
  const programNameInputs = document.querySelectorAll('.programName')
  programNameInputs.forEach((programName) => {
    programName.onchange = (e) => {
      educationItemValuesMap.delete(e.target.id)
      educationItemValuesMap.set(e.target.id, e.target.value)
      console.log(e.target.id, ':', educationItemValuesMap.get(e.target.id))
    }
  })
  const startYearInputs = document.querySelectorAll('.schoolStartYear')
  startYearInputs.forEach((startYear) => {
    startYear.onchange = (e) => {
      educationItemValuesMap.delete(e.target.id)
      educationItemValuesMap.set(e.target.id, e.target.value)
      console.log(e.target.id, ':', educationItemValuesMap.get(e.target.id))
    }
  })
  const endYearInputs = document.querySelectorAll('.schoolEndYear')
  endYearInputs.forEach((endYear) => {
    endYear.onchange = (e) => {
      educationItemValuesMap.delete(e.target.id)
      educationItemValuesMap.set(e.target.id, e.target.value)
      console.log(e.target.id, ':', educationItemValuesMap.get(e.target.id))
    }
  })
}
const addEducationItem = (id) => {
  educationItemsMap.set(`educationItem${id}`, { html: newEducationItem(id) })
}

const setEducationDelBtnEventListener = () => {
  const deleteBtns = document.querySelectorAll(`.educationDelBtn`)
  deleteBtns.forEach((delBtn) => {
    delBtn.onclick = (e) => {
      deleteEducationItem(e)
    }
  })
}

const renderEducation = () => {
  educationContainer.innerHTML = ''
  educationItemsMap.forEach((item) => {
    educationContainer.innerHTML += item.html
  })
  getEducationValue()
  setEducationDelBtnEventListener()
}

const deleteEducationItem = (e) => {
  const id = e.target.id.slice(15)
  console.log(id)
  educationItemsMap.delete(`educationItem${id}`)
  setEducationValue()
  renderEducation()
}

const setEducationValue = () => {
  educationItemsMap.forEach((value, key) => {
    const id = key.slice(13)
    console.log({ 'educationItem Id': id })
    const schoolName = educationItemValuesMap.get(`schoolName${id}`)
    const programName = educationItemValuesMap.get(`programName${id}`)
    const schoolStartYear = educationItemValuesMap.get(`schoolStartYear${id}`)
    const schoolEndYear = educationItemValuesMap.get(`schoolEndYear${id}`)
    console.log(
      'setting map : ',
      schoolName,
      ',',
      programName,
      ',',
      schoolStartYear,
      ',',
      schoolEndYear
    )
    educationItemsMap.set(`educationItem${id}`, {
      html: newEducationItem(
        id,
        false,
        schoolName,
        programName,
        schoolStartYear,
        schoolEndYear
      ),
    })
    console.log(educationItemsMap.get(`educationItem${id}`))
  })
}

const newEducationItem = (
  id,
  hidden,
  schoolName,
  programName,
  schoolStartYear,
  schoolEndYear
) => {
  return /*html*/ `
<li class="educationItem${id} formItem">
              <div>
                <input
                  type="text"
                  ${schoolName ? `value="${schoolName}"` : `value=""`}
                  class="schoolName"
                  name="schoolName"
                  id="schoolName${id}"
                  placeholder="School Name"
                />
                <button type="button" ${
                  hidden ? `hidden = true` : null
                } class="educationDelBtn" id="educationDelBtn${id}">X</button>
              </div>
              <div>
                <input
                  type="text"
                  ${programName ? `value="${programName}"` : `value=""`}
                  class="programName"
                  name="programName"
                  id="programName${id}"
                  placeholder="Program Name"
                />
              </div>
              <div class="year">
                <div>
                  <input
                    type="text"
                    class="schoolStartYear"
                    ${
                      schoolStartYear
                        ? `value="${schoolStartYear}"`
                        : `value=""`
                    }     
                    name="schoolStartYear"
                    id="schoolStartYear${id}"
                    placeholder="Start Year - yyyy"
                  />-
                </div>
                <div>
                  <input
                    type="text"
                    class="schoolEndYear"
                    ${schoolEndYear ? `value="${schoolEndYear}"` : `value=""`}
                    name="schoolEndYear"
                    id="schoolEndYear${id}"
                    placeholder="End Year - yyyy"
                  />
                </div>
              </div>
            </li>
            
`
}
//======================================================================================================

let workItemId = 0
let workItemsMap = new Map()
let workItemValuesMap = new Map()
const workContainer = document.querySelector('.work-input-container')
const addWork = document.querySelector('#addWork')
const delBtn = /*html*/ `<svg xmlns="http://www.w3.org/2000/svg" fill="currentColor" class="bi bi-x-circle" viewBox="0 0 16 16">
<path d="M8 15A7 7 0 1 1 8 1a7 7 0 0 1 0 14zm0 1A8 8 0 1 0 8 0a8 8 0 0 0 0 16z"/>
<path d="M4.646 4.646a.5.5 0 0 1 .708 0L8 7.293l2.646-2.647a.5.5 0 0 1 .708.708L8.707 8l2.647 2.646a.5.5 0 0 1-.708.708L8 8.707l-2.646 2.647a.5.5 0 0 1-.708-.708L7.293 8 4.646 5.354a.5.5 0 0 1 0-.708z"/>
</svg>`

addWork.onclick = (e) => {
  workItemId += 1
  addWorkItem(workItemId)
  setWorkValue()
  renderWork()
}

const workMapInit = () => {
  getWorkValue()
  workItemsMap.set(`workItem0`, { html: newWorkItem(0, true) })
}
const getWorkValue = () => {
  const jobTitleInputs = document.querySelectorAll('.jobTitle')
  jobTitleInputs.forEach((jobTitle) => {
    jobTitle.onchange = (e) => {
      workItemValuesMap.delete(e.target.id)
      workItemValuesMap.set(e.target.id, e.target.value)
      console.log(e.target.id, ':', workItemValuesMap.get(e.target.id))
    }
  })
  const companyNameInputs = document.querySelectorAll('.companyName')
  companyNameInputs.forEach((companyName) => {
    companyName.onchange = (e) => {
      workItemValuesMap.delete(e.target.id)
      workItemValuesMap.set(e.target.id, e.target.value)
      console.log(e.target.id, ':', workItemValuesMap.get(e.target.id))
    }
  })
  const companyStartYearInputs = document.querySelectorAll('.companyStartYear')
  companyStartYearInputs.forEach((companyStartYear) => {
    companyStartYear.onchange = (e) => {
      workItemValuesMap.delete(e.target.id)
      workItemValuesMap.set(e.target.id, e.target.value)
      console.log(e.target.id, ':', workItemValuesMap.get(e.target.id))
    }
  })

  const companyEndYearInputs = document.querySelectorAll('.companyEndYear')
  companyEndYearInputs.forEach((companyEndYear) => {
    companyEndYear.onchange = (e) => {
      workItemValuesMap.delete(e.target.id)
      workItemValuesMap.set(e.target.id, e.target.value)
      console.log(e.target.id, ':', workItemValuesMap.get(e.target.id))
    }
  })
}
const addWorkItem = (id) => {
  workItemsMap.set(`workItem${id}`, { html: newWorkItem(id) })
}
const setWorkDelBtnEventListener = () => {
  const deleteBtns = document.querySelectorAll(`.workDelBtn`)
  deleteBtns.forEach((delBtn) => {
    delBtn.onclick = (e) => {
      deleteWorkItem(e)
    }
  })
}
const renderWork = () => {
  workContainer.innerHTML = ''
  workItemsMap.forEach((item) => {
    workContainer.innerHTML += item.html
  })
  getWorkValue()
  setWorkDelBtnEventListener()
}

const deleteWorkItem = (e) => {
  const id = e.target.id.slice(10)
  console.log(e.target.id)
  workItemsMap.delete(`workItem${id}`)
  setWorkValue()
  renderWork()
}

const setWorkValue = () => {
  workItemsMap.forEach((value, key) => {
    const id = key.slice(8)
    console.log({ 'workItem Id': id })

    const jobTitle = workItemValuesMap.get(`jobTitle${id}`)
    const companyName = workItemValuesMap.get(`companyName${id}`)
    const companyStartYear = workItemValuesMap.get(`companyStartYear${id}`)
    const companyEndYear = workItemValuesMap.get(`companyEndYear${id}`)
    workItemsMap.set(`workItem${id}`, {
      html: newWorkItem(
        id,
        false,
        jobTitle,
        companyName,
        companyStartYear,
        companyEndYear
      ),
    })
    console.log(workItemsMap.get(`workItem${id}`))
  })
}
const newWorkItem = (
  id,
  hidden,
  jobTitle,
  companyName,
  companyStartYear,
  companyEndYear
) => {
  return /*html*/ `
<li class="workItem${id} formItem">
              <div>
                <input
                  type="text"
                  class="jobTitle"
                  ${jobTitle ? `value="${jobTitle}"` : `value=""`}
                  name="jobTitle"
                  id="jobTitle${id}"
                  placeholder="Job Title"
                />
                <button type="button" ${
                  hidden ? `hidden = true` : null
                } class="workDelBtn" id="workDelBtn${id}">
                X
                </button>
              </div>
              <div>
                <input
                  type="text"
                  class="companyName"
                  ${companyName ? `value="${companyName}"` : `value=""`}
                  name="companyName"
                  id="companyName${id}"
                  placeholder="Company Name"
                />
              </div>
              <div class="year">
                <div>
                  <input
                    type="text"
                    class="companyStartYear"
                    ${
                      companyStartYear
                        ? `value="${companyStartYear}"`
                        : `value=""`
                    }
                    name="companyStartYear"
                    id="companyStartYear${id}"
                    placeholder="Start Year - yyyy"
                  />-
                </div>
                <div>
                  <input
                    type="text"
                    class="companyEndYear"
                    ${companyEndYear ? `value="${companyEndYear}"` : `value=""`}
                    name="companyEndYear"
                    id="companyEndYear${id}"
                    placeholder="End Year - yyyy"
                  />
                </div>
              </div>
            </li>
  `
}

//======================================================================================================

let skillItemId = 0
let skillItemsMap = new Map()
let skillItemValuesMap = new Map()

const skillContainer = document.querySelector('.skillSetContainer')
const skillBtnContainer = document.querySelector('.skill-container')
// document.querySelectorAll('.')

const getSkillSet = async () => {
  const res = await fetch('/skillSet')
  const skillSet = await res.json()
  return skillSet
}
const renderSkillSet = (skillSet) => {
  skillContainer.innerHTML = ''
  skillSet.forEach((skill) => {
    skillItemsMap.set(`skillitem${skill.id}`, { checked: false })
    console.log(skillItemsMap.get(`skillitem${skill.id}`))
    skillContainer.innerHTML +=
      /*html*/
      `
      <div>
        <input type="checkbox" name="skills" id="skill${skill.id}" class="skill" value="${skill.id}"/>${skill.skill_name}
      </div>
      `
  })
  addSkillEventListener()
}
const addSkillEventListener = () => {
  const skills = document.querySelectorAll('.skill')
  skills.forEach((skill) => {
    skill.addEventListener('change', function () {
      console.log('checked')
      skillItemsMap.set(skill.id, { checked: true })
      console.log(
        'skill.innerHTML: ',
        skillItemsMap.get(`skillitem${skill.id}`)
      )
    })
  })
}
const renderSkillContainerBtns = () => {
  skillBtnContainer.innerHTML +=
    /*html*/
    `<div class="buttons">
      <button type="button" id="backToWorkExperience">Back</button>
      <button type="submit" id="submittion">Submit</button>
      </div>
    `
  backToWorkExperienceEventListener()
}

//======================================================================================================

const profileForm = document.querySelector('#profileForm')
profileForm.addEventListener('submit', async (e) => {
  e.preventDefault()
  const formData = new FormData(profileForm)
  for (let data of formData.entries()) {
    console.log(data[0], ' ', data[1])
  }
  const res = await fetch('/cv', {
    method: 'POST',
    body: formData,
  })
  if (res.status === 200) {
    console.log('Success')
    // profileForm.reset()
    // todo
    const urlParam = window.location.search.substring(1)
    window.location.href = urlParam + `.html`
    // console.log(url)
  } else {
    const json = await res.json()
    alert(json)
  }
})
