const addBtn = document.querySelector('#additem')
const itemContainer = document.querySelector('.items-container')

let itemId = 0
let itemsMap = new Map()
let itemValuesMap = new Map()

addBtn.onclick = (e) => {
  itemId += 1
  addItem(itemId)
  setDelBtnEventListener()
  setValue()
}

const setDelBtnEventListener = () => {
  const deleteBtns = document.querySelectorAll(`.delBtn`)
  deleteBtns.forEach((delBtn) => {
    delBtn.onclick = (e) => {
      deleteItem(e)
    }
  })
}

const setValue = () => {
  const names = document.querySelectorAll('.name')
  names.forEach((name) => {
    name.onchange = (e) => {
      itemValuesMap.delete(e.target.id)
      itemValuesMap.set(e.target.id, e.target.value)
      updateMap()
    }
  })
  const years = document.querySelectorAll('.year')
  years.forEach((year) => {
    year.onchange = (e) => {
      itemValuesMap.delete(e.target.id)
      itemValuesMap.set(e.target.id, e.target.value)
      updateMap()
    }
  })
}

const updateMap = () => {
  itemsMap.forEach((value, key) => {
    const id = key.slice(4)
    const name = itemValuesMap.get(`name${id}`)
    const year = itemValuesMap.get(`year${id}`)
    itemsMap.set(`item${id}`, { html: newItem(id, name, year) })
  })
}

const deleteItem = (e) => {
  const id = e.target.id.slice(6)
  console.log(id)
  itemsMap.delete(`item${id}`)
  render()
}

const addItem = (id) => {
  itemsMap.set(`item${id}`, { html: newItem(id) })
  render()
}

const render = () => {
  itemContainer.innerHTML = ''
  itemsMap.forEach((item) => {
    itemContainer.innerHTML += item.html
  })
  setDelBtnEventListener()
}

const newItem = (id, name, year) => {
  return (
    /*html*/
    `
                <li class="item${id}">
                    <input type="text" class="name" id="name${id}" placeholder="name" ${
      name ? `value=${name}` : null
    }>
                    <input type="text" class="year" id="year${id}" placeholder="year" ${
      year ? `value=${year}` : null
    }>
                    <button type="button" class="delBtn" id="delBtn${id}">delete item</button>
                </li>
    `
  )
}
