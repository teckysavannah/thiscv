import { header } from './navbar.js'
import { loadCurrentUser } from '../currentUser.js'

window.onload = async () => {
  header()
  loadCurrentUser()
  await loadData()
}

async function loadData() {
  const res = await fetch('/getUserInfo')
  const json = await res.json()
  console.log(json.users)
  const user = json.users
  firstname.innerHTML = user.firstname
  lastname.innerHTML = user.lastname
  address.innerHTML = user.address
  email.innerHTML = user.email
  phonenumber.innerHTML = user.phone_number
  if (user.image != '') {
    userIcon.innerHTML =
      /*html*/
      `
      <div class="avatar">
      <img 
        src="../uploads/${user.image}"
        alt=""
      />
      </div>
  `
  }

  const educationInfo = (schoolName, programName, startYear, endYear) => {
    return (
      /*html*/
      `
      <div class="timeline-block">
      <h1 id="schoolname">${schoolName}</h1>
      <p id="programname">${programName}</p>
      <time>
      <span id="schoolstartyear">${startYear}</span> -
      <span id="schoolendyear">${endYear}</span>
      </time>
      </div>
      `
    )
  }
  const educationdetails = document.querySelector('#educationContainer')
  for (let i = 0; i < user.all_exp[0].education.length; i++) {
    educationdetails.innerHTML += educationInfo(
      user.all_exp[0].education[i].school_name,
      user.all_exp[0].education[i].program_name,
      user.all_exp[0].education[i].start_year,
      user.all_exp[0].education[i].end_year
    )
  }

  const workInfo = (companyName, title, startYear, endYear) => {
    return (
      /*html*/
      `
      <div class="timeline-block">
      <h1 id="companyname">${companyName}</h1>
      <p id="jobtitle">${title}</p>
      <time>
      <span id="jobstartyear">${startYear}</span> -
      <span id="jobendyear">${endYear}</span>
      </time>
      </div>
      `
    )
  }
  const workDetails = document.querySelector('#workContainer')
  for (let i = 0; i < user.all_exp[0].work.length; i++) {
    workDetails.innerHTML += workInfo(
      user.all_exp[0].work[i].company_name,
      user.all_exp[0].work[i].job_title,
      user.all_exp[0].work[i].start_year,
      user.all_exp[0].work[i].end_year
    )
  }
  //description
const descriptionDetails = document.querySelector('#description')
descriptionDetails.innerHTML +=
  /*html*/
  `
  <h1 class="title">about me</h1>
          <p class="paragraph">${user.description}</p>
  
  `
  //skills
  const skills = document.querySelector('.skills__item')
  for (let skill of user.all_exp[0].user_cv_skills) {

    console.log(skill)
    skills.innerHTML +=
      /*html*/
      `
      <li>
      <svg viewbox="0 0 100 100">
        <circle cx="50" cy="50" r="45"></circle>
        <circle
          class="cbar"
          cx="50"
          cy="50"
          r="45"
          style="--percent: 0.6"
        ></circle>
      </svg>
      <span class="tl-name">${skill}</span>
      <span class="tl-exp">100%</span>
    </li>

    `
  }
}
