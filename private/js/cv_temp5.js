import { header } from './navbar.js'
import { loadCurrentUser } from '../currentUser.js'
import { loadTemplateData } from './getUserTemplateInfo.js'
window.onload = async () => {
  header()
  await loadCurrentUser()
}
const userTemplate = await loadTemplateData()
console.log(userTemplate)
//user Contact
firstname.innerHTML = userTemplate.firstname
lastname.innerHTML = userTemplate.lastname
address.innerHTML = userTemplate.address
email.innerHTML = userTemplate.email
phonenumber.innerHTML = userTemplate.phone_number
if (userTemplate.image != '') {
  userIcon.innerHTML =
    /*html*/
    `
    <div class="avatar">
    <img 
      src="../uploads/${user.image}"
      alt=""
    />
    </div>
`
}

//description
const descriptionDetails = document.querySelector('#description')
descriptionDetails.innerHTML +=
  /*html*/
  `<span class="description">${userTemplate.description}</span>
  `
//education
const educationdetails = document.querySelector('#educationContainer')
for (let i = 0; i < userTemplate.all_exp[0].education.length; i++) {
  educationdetails.innerHTML += educationInfo(
    userTemplate.all_exp[0].education[i].school_name,
    userTemplate.all_exp[0].education[i].program_name,
    userTemplate.all_exp[0].education[i].start_year,
    userTemplate.all_exp[0].education[i].end_year
  )
}
//work
const workDetails = document.querySelector('#workContainer')
for (let i = 0; i < userTemplate.all_exp[0].work.length; i++) {
  workDetails.innerHTML += workInfo(
    userTemplate.all_exp[0].work[i].company_name,
    userTemplate.all_exp[0].work[i].job_title,
    userTemplate.all_exp[0].work[i].start_year,
    userTemplate.all_exp[0].work[i].end_year
  )
}
//skills
const skills = document.querySelector('#skills')
for (let skill of userTemplate.all_exp[0].user_cv_skills) {
  console.log(skill)
  skills.innerHTML +=
    /*html*/
    `<div class="skill-item">${skill}</div>
    `
}

function educationInfo(schoolName, programName, startYear, endYear) {
  return (
    /*html*/
    `
    <div class="cv-content-item">
    <div class="title" id="schoolname">${schoolName}</div>
    <div class="subtitle" id="programname">${programName}</div>
    <div class="time">
      <span id="schoolstartyear">${startYear}</span> -
      <span id="schoolendyear">${endYear}</span>
    </div>
  </div>
          `
  )
}
function workInfo(companyName, title, startYear, endYear) {
  return (
    /*html*/
    `
    <div class="cv-content-item">
    <div class="title" id="jobtitle">${title}</div>
    <div class="subtitle" id="companyname">${companyName}</div>
    <div class="time">
      <span id="jobstartyear">${startYear}</span> -
      <span id="jobendyear">${endYear}</span>
    </div>
  </div>
          `
  )
}
