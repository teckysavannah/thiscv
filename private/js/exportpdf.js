const pdf = document.getElementById('exportPDF')
pdf.addEventListener('click', function () {
  var doc = new jsPDF()
  // var width = doc.internal.pageSize.getWidth
  // var height = doc.internal.pageSize.getHeight
  html2canvas(document.body, {
    onrendered: function (canvas) {
      var image = canvas.toDataURL('image/png')
      doc.addImage(image, 'JPEG', 0, 0)
      doc.save('test.pdf')
    },
  })
})


