export function header() {
  document.querySelector('header').innerHTML = /*html*/ `
    <div class="nav">
    <div class="brand">
    <a href="index.html"></a>
      <a href="index.html"> <img class="logo" src="" alt="Logo"></a> 
    </div>
    <nav>
      <ul>
        <li><a href="about.html">About</a></li>
        <li><a href="contact.html">Contact</a></li>
        <li><a href="templateInputDetails.html">CV Templates</a></li>
        <li><a href="question.html">Mock Interview</a></li>
        <li class="login-btn"><a href="login.html">Login</a></li>
        <li class="logout-btn display-none"><a href="/logout">Logout</a></li>
      
      </ul>
    </nav>
    <i class="fas fa-globe-americas"></i>
    <!-- <img class="menu-bar" src="menu.png" alt="Menu"> -->
  </div>
  `
  const menuBtn = document.querySelector('.fa-globe-americas')
  const navbar = document.querySelector('nav')
  // console.log(navbar.classList)
  menuBtn.addEventListener('click', () => {
    navbar.classList.toggle('show')
  })

  const logoutBtn = document.querySelector('.logout-btn')
  logoutBtn.addEventListener('click', async () => {
    await fetch('/logout')
    // console.log("hi")
  })
}
