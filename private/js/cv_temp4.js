// const firstName = document.querySelector('#first-name')
// const lastName = document.querySelector('#last-name')
import { header } from './navbar.js'
import { loadCurrentUser } from '../currentUser.js'

window.onload = async () => {
  header()
  loadCurrentUser()
  await loadData()
}

async function loadData() {
  const res = await fetch('/getUserInfo')
  const json = await res.json()
  if (res.status === 400) {
    alert(json.message)
  }
  console.log(json.users)
  const user = json.users

  firstname.innerHTML = user.firstname
  lastname.innerHTML = user.lastname
  address.innerHTML = user.address
  email.innerHTML = user.email
  phonenumber.innerHTML = user.phone_number
  if (user.image != '') {
    userIcon.innerHTML =
      /*html*/
      `
  <div class="avatar">
  <img 
    src="../uploads/${user.image}"
    alt=""
  />
  </div>
  `
  }

  const educationInfo = (schoolName, programName, startYear, endYear) => {
    return (
      /*html*/
      `
      <ul>
      <li>
          <div><span class="msg-1" id="schoolstartyear">${startYear}</span>  - 
          <span id="schoolendyear">${endYear}</span> | <span id="schoolname">${schoolName}</span></div>
          <div class="msg-2" id="programname">${programName}</div>
      </li>
  </ul>
      `
    )
  }
  const educationdetails = document.querySelector('#educationContainer')
  for (let i = 0; i < user.all_exp[0].education.length; i++) {
    educationdetails.innerHTML += educationInfo(
      user.all_exp[0].education[i].school_name,
      user.all_exp[0].education[i].program_name,
      user.all_exp[0].education[i].start_year,
      user.all_exp[0].education[i].end_year
    )
  }

  const workInfo = (companyName, title, startYear, endYear) => {
    return (
      /*html*/
      `
      <ul>
      <li>
          <div><span class="msg-1" id="jobstartyear">${startYear}</span> - 
          <span id="jobendyear">${endYear}</span></div>
          <div class="msg-2" id="companyname">${companyName}</div>
          <div class="msg-3" id=jobtitle>${title}</div>
      </li>
  </ul>
      `
    )
  }
  const workDetails = document.querySelector('#workContainer')
  for (let i = 0; i < user.all_exp[0].work.length; i++) {
    workDetails.innerHTML += workInfo(
      user.all_exp[0].work[i].company_name,
      user.all_exp[0].work[i].job_title,
      user.all_exp[0].work[i].start_year,
      user.all_exp[0].work[i].end_year
    )
  }

  //skills
  const skills = document.querySelector('#skills__item')
  for (let skill of user.all_exp[0].user_cv_skills) {
    console.log(skill)
    skills.innerHTML +=
      /*html*/
      `
      <div class="skill-1" id="#skills__item">
      <p>${skill}</p>
      <div class="progress">
          <div class="dot active"></div>
          <div class="dot active"></div>
          <div class="dot active"></div>
          <div class="dot active"></div>
          <div class="dot active"></div>
          <div class="dot active"></div>
          <div class="dot active"></div>
      </div>
  </div>
  `
  }
  //description
  const descriptionDetails = document.querySelector('#description')

  descriptionDetails.innerHTML +=
    /*html*/
    `
    <div class="desc" id="description">${user.description}</div>
    `
}

//
