export async function loadTemplateData() {
    const res = await fetch('/getUserInfo')
    const json = await res.json()
    if (res.status === 400) {
      alert(json.message)
    }
    const userTempInfo = json.users
    return userTempInfo
}