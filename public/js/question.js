import { header } from './navbar.js'
import { loadCurrentUser } from './currentUser.js'
// import { footer } from './footer.js'

window.onload = async () => {
  header()
  loadCurrentUser()
  await number()
}

async function number() {
  const resp = await fetch('/question')
  const questions = await resp.json()
  // console.log(questions)

  let htmlStr = ``
  for (const qt of questions) {
    const quest = qt.question
    const questionid = qt.id

    htmlStr += /*HTML*/ `
    <button class="box" data-id="${questionid}">${questionid}</button>
    `
  }

  document.querySelector('.menu').innerHTML = htmlStr

  let Slid = /*HTML*/ `
    <h1 class="h1" id="h1">${questions[0].question}</h1>`

  document.querySelector('.slid').innerHTML = Slid

  let boxes = document.querySelectorAll('.box')
  // console.log(boxes)
  for (const box of boxes) {
    box.addEventListener('click', async () => {
      const boxid = box.getAttribute('data-id')
      const res = await fetch(`/question/${boxid}`, {
        method: 'GET',
        headers: { 'Content-Type': 'application/json' },
      })

      const json = await res.json()
      // console.log(json)

      let html = ``
      htmlStr = /*HTML*/ `
    <h1 class="h1" id="h1">${json[0].question}</h1>`
      document.querySelector('.slid').innerHTML = htmlStr
    })
  }
}
