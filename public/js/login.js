import { loadCurrentUser } from './currentUser.js'
import { header } from './navbar.js'
import {footer} from './footer.js'
window.onload = () => {
  header()
  footer()
  loadCurrentUser()
}

const loginForm = document.querySelector('#loginForm')
const loginEmail = document.querySelector('#loginEmail')
const loginPassword = document.querySelector('#loginPassword')

// Login Form Submission
loginForm.addEventListener('submit', async (e) => {
  e.preventDefault()
  const email = loginEmail.value
  const password = loginPassword.value
  const formObject = { email, password }
  const res = await fetch(`/login`, {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json',
    },
    body: JSON.stringify(formObject),
  })
  loginForm.reset()
  const result = await res.json()
  if (res.status === 200) {
    window.location = '/'
  } else {
    alert(result.message)
  }
})

// const hideContentBtn = document.querySelector('#test-hidden')
// hideContentBtn.onclick = () => {
//   loginForm.classList.toggle('display-none')
// }
