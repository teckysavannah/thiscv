let mediaRecorder
let recordedBlobs

const video = document.querySelector('video')
const codecPreferences = document.querySelector('#codecPreferences')
const errorMsgElement = document.querySelector('span#errorMsg')
const recordedVideo = document.querySelector('video#recorded')
const recordButton = document.querySelector('button#record')

let startTime = null
let lastKey = null
let i = 0

// a.i. model
async function initModel() {
    let a = await faceapi.nets.ssdMobilenetv1.loadFromUri('/models')
    let b = await faceapi.nets.faceExpressionNet.loadFromUri('/models')

    loopFunc()
}

var requestAnimationFrameCross =
    window.requestAnimationFrame ||
    window.mozRequestAnimationFrame ||
    window.oRequestAnimationFrame ||
    window.msRequestAnimationFrame

let totalCount = {
    happy: 0,
    sad: 0,
    angry: 0,
    disgusted: 0,
    surprised: 0,
    fearful: 0,
    neutral: 0,
}
async function loopFunc() {
    //const detections = await faceapi.detectAllFaces(input)

    const detections = await faceapi.detectAllFaces(video).withFaceExpressions()
    // const detections = await faceapi.detectSingleFace(video).withFaceExpressions()

    //console.log(detections);

    function argMax(array) {
        return array
            .map((x, i) => [x, i])
            .reduce((r, a) => (a[0] > r[0] ? a : r))[1]
    }

    if (detections.length > 0) {
        let arr = [
            detections[0].expressions.happy,
            detections[0].expressions.sad,
            detections[0].expressions.angry,
            detections[0].expressions.disgusted,
            detections[0].expressions.surprised,
            detections[0].expressions.fearful,
            detections[0].expressions.neutral,
        ]

        let result = argMax(arr)

        // Jason code need learn later
        let names = [
            'happy',
            'sad',
            'angry',
            'disgusted',
            'surprised',
            'fearful',
            'neutral',
        ]
        const key = names[result]
        totalCount[key] += 1

        // compare with last result
        // get new Date()

        document.querySelector('.emotionResult').innerHTML = ' You start showing the ' + '<b>' + key + '</b>' + ' face.'

        if (startTime) {
            const relate = document.querySelector('.change')
            console.log(relate)
            if (lastKey === null || lastKey !== key) {
                console.log(lastKey, key)
                lastKey = key
                const cur = new Date().getTime() / 1000
                console.log(cur - startTime)
                relate.innerHTML = 'After ' + (Math.floor(cur - startTime)) + ' seconds,'
            }
        }
        // console.log(totalCount)
        // console.log(detections[0].expressions);

    }
    requestAnimationFrameCross(loopFunc)
}

async function onCapture() {
    await navigator.mediaDevices
        .getUserMedia(constraints)
        .then(handleSuccess)
        .catch(handleError)
}

async function handleSuccess(stream) {
    window.stream = stream
    video.srcObject = stream
}

async function handleError(error) {
    console.log(
        'navigator.MediaDevices.getUserMedia error: ',
        error.message,
        error.name
    )
}

// async function onCapture() {
//     navigator.mediaDevices.getUserMedia(constraints).then(handleSuccess).catch(handleError);
// }
// onCapture()

video.addEventListener('loadeddata', async (event) => {
    console.log("let'start")
    initModel()
})

// WebRTC samples MediaRecorder
function handleDataAvailable(event) {
    console.log('handleDataAvailable', event)
    if (event.data && event.data.size > 0) {
        recordedBlobs.push(event.data)
    }
}

function getSupportedMimeTypes() {
    const possibleTypes = [
        'video/webm;codecs=vp9,opus',
        'video/webm;codecs=vp8,opus',
        'video/webm;codecs=h264,opus',
        'video/mp4;codecs=h264,aac',
    ]
    return possibleTypes.filter((mimeType) => {
        return MediaRecorder.isTypeSupported(mimeType)
    })
}

function handleSuccess(stream) {
    recordButton.disabled = false
    console.log('getUserMedia() got stream:', stream)
    window.stream = stream

    const gumVideo = document.querySelector('video#gum')
    gumVideo.srcObject = stream

    getSupportedMimeTypes().forEach((mimeType) => {
        const option = document.createElement('option')
        option.value = mimeType
        option.innerText = option.value
        codecPreferences.appendChild(option)
    })
    codecPreferences.disabled = false
}

async function init(constraints) {
    try {
        const stream = await navigator.mediaDevices.getUserMedia(constraints)
        handleSuccess(stream)
    } catch (e) {
        console.error('navigator.getUserMedia error:', e)
        errorMsgElement.innerHTML = `navigator.getUserMedia error:${e.toString()}`
    }
}

// open camera
document.querySelector('button#start').addEventListener('click', async () => {
    const hasEchoCancellation =
        document.querySelector('#echoCancellation').checked
    const constraints = {
        audio: {
            echoCancellation: { exact: hasEchoCancellation },
        },
        video: {
            width: 550,
            height: 520,
        },
    }
    console.log('Using media constraints:', constraints)
    await init(constraints)
})

// close camera
async function stopMedia() {
    if (window.stream) {
        const videoStreams = window.stream.getVideoTracks()

        videoStreams.forEach((stream) => {
            stream.stop() // stop all media stream
        })

            // release resources
            ; (startTime = null), (lastKey = null)
        video.src = video.srcObject = null
    }
}

// start record
recordButton.addEventListener('click', () => {
    if (recordButton.textContent === 'Start Recording') {
        startRecording()
    } else {
        stopRecording()
        recordButton.textContent = 'Start Recording'
        playButton.disabled = false
        downloadButton.disabled = false
        codecPreferences.disabled = false
    }
})

function startRecording() {
    console.log(
        '========================== startRecording =========================='
    )
    startTime = new Date().getTime() / 1000
    const date = new Date()
    let ss = document.querySelector('.ss');
    setInterval(function () {
        i += 1,
            ss.innerHTML = "【 Timer: " + '<b>' + i + '</b>' + " second 】"
        console.log(date.getSeconds())
    }, 1000)

    recordedBlobs = []
    const mimeType =
        codecPreferences.options[codecPreferences.selectedIndex].value
    const options = { mimeType }

    try {
        mediaRecorder = new MediaRecorder(window.stream, options)
    } catch (e) {
        console.error('Exception while creating MediaRecorder:', e)
        errorMsgElement.innerHTML = `Exception while creating MediaRecorder: ${JSON.stringify(
            e
        )}`
        return
    }

    console.log('Created MediaRecorder', mediaRecorder, 'with options', options)
    recordButton.textContent = 'Stop Recording'
    playButton.disabled = true
    downloadButton.disabled = true
    codecPreferences.disabled = true
    mediaRecorder.onstop = (event) => {
        console.log('Recorder stopped: ', event)
        console.log('Recorded Blobs: ', recordedBlobs)
    }
    mediaRecorder.ondataavailable = handleDataAvailable
    mediaRecorder.start()
    console.log('MediaRecorder started', mediaRecorder)
}

function stopRecording() {
    console.log("========================== stopRecording ==========================")
    startTime = null
    lastKey = null
    console.log(totalCount)
    let rank1 = document.querySelector("#rank1")
    let rank2 = document.querySelector("#rank2")
    let rank3 = document.querySelector("#rank3")
    let rank4 = document.querySelector("#rank4")
    let rank5 = document.querySelector("#rank5")
    let rank6 = document.querySelector("#rank6")
    let rank7 = document.querySelector("#rank7")
    const totalCountArr = Object.keys(totalCount)
    let valueArr = totalCountArr.map((key) => { return key + " : " + totalCount[key] })
    console.log(valueArr)
    rank1.innerHTML = valueArr[0]
    rank2.innerHTML = valueArr[1]
    rank3.innerHTML = valueArr[2]
    rank4.innerHTML = valueArr[3]
    rank5.innerHTML = valueArr[4]
    rank6.innerHTML = valueArr[5]
    rank7.innerHTML = valueArr[6]
}

// play record Video
const playButton = document.querySelector('button#play')
playButton.addEventListener('click', () => {
    const mimeType = codecPreferences.options[codecPreferences.selectedIndex].value.split(';', 1)[0];
    const superBuffer = new Blob(recordedBlobs, { type: mimeType });
    recordedVideo.src = null;
    recordedVideo.srcObject = null;
    recordedVideo.src = window.URL.createObjectURL(superBuffer);
    recordedVideo.controls = true;
    recordedVideo.play();
});

// Object.keys(totalCount. happy)[0, 1, 2, 3, 4, 5, 6];

// download video
const downloadButton = document.querySelector('button#download')
downloadButton.addEventListener('click', () => {
    const blob = new Blob(recordedBlobs, { type: 'video/webm' })
    const url = window.URL.createObjectURL(blob)
    const a = document.createElement('a')
    a.style.display = 'none'
    a.href = url
    a.download = 'test.webm'
    document.body.appendChild(a)
    a.click()
    setTimeout(() => {
        document.body.removeChild(a)
        window.URL.revokeObjectURL(url)
    }, 100)
})
