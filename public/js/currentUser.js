export async function loadCurrentUser() {
  // const currentUser = document.querySelector('.currentUser')
  const navbar_loginBtn = document.querySelector('.login-btn')
  const navbar_logoutBtn = document.querySelector('.logout-btn')

  console.log('Loading current user')
  const res = await fetch('/loadCurrentUser')
  const user = await res.json()
  console.log(user)
  if (res.status === 200) {
    // currentUser.innerHTML = user.userName
    navbar_loginBtn.classList.add('display-none')
    navbar_logoutBtn.classList.remove('display-none')
  } else {
    console.log('No user')
    // currentUser.innerHTML = 'Please login first'
    navbar_logoutBtn.classList.add('display-none')
    navbar_loginBtn.classList.remove('display-none')
  }
}
