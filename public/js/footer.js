export function footer() {
  document.querySelector('footer').innerHTML = /*html*/ `

    <div class="footer">

    
    <div class="column">
    <ul>
    <li class="title">OTHER LINKS</li>
    <li><a href="#">Terms & Conditions</a></li>
    <li><a href="#">Privacy Policy</a></li>
    <!--<li><a href="#">Cookie Policy</a></li>
    <li><a href="#">Tickets</a></li>-->
    </ul>
    </div>
    
    <div class="column">
    <ul>
    <li class="title">SHORT CUT</li>
    <li><a href="#">Contact Us</a></li>
    <!--<li><a href="#">Our Services</a></li>
    <li><a href="#">Our Mission</a></li>-->
    <li><a href="#">About Us</a></li>
    </ul>
    </div>
    
    <div class="column">
    <ul>
    <li class="title">NEWSLETTER</li>
    <li>
    <form action="#" method="post">
    <input type="email" name="email" placeholder="Email*" maxlength="80" required />
    </form>
    </li>
    <li>
    <i class="fa fa-paper-plane sdEmailBtn "></i>
    <a href="#" title="Address,State,Country,Pincode"><i class="fa fa-map-marker"></i></a>
    <a href="#"><i class="fa fa-phone"></i></a>
    <a href="#"><i class="fa fa-envelope"></i></a>
    </li>
    </ul>
    </div>
    </div>
    <div class="sub-footer">
    C17_BAD_PROJECT || MINT || RYAN || SAVANNAH
    </div>
`
}
