import { loadCurrentUser } from './currentUser.js'
import { header } from './navbar.js'
import {footer}from './footer.js'

window.onload = () => {
  header()
  footer()
  loadCurrentUser()
}


const cvtemplates=document.querySelectorAll('.CVtemplates')
cvtemplates.onclick = async(e) =>{
    const templateId=e.target.id.slice('10')
    console.log({templateId})
    await fetch(`/createCV/${e.target.id}`)
}
