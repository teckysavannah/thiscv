import { loadCurrentUser } from './currentUser.js'
import { header } from './navbar.js'
import {footer}from './footer.js'

window.onload = () => {
  header()
  footer()
  loadCurrentUser()
}