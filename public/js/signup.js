import { loadCurrentUser } from './currentUser.js'
import { header } from './navbar.js'
import {footer} from './footer.js'
window.onload = () => {
  header()
  footer()
  loadCurrentUser()
}

// const loginForm = document.querySelector('#loginForm')
// const loginEmail = document.querySelector('#loginEmail')
// const loginPassword = document.querySelector('#loginPassword')
const registerForm = document.querySelector('#registerForm')
const userName = document.querySelector('#registerUsername')
const userEmail = document.querySelector('#registerEmail')
const password = document.querySelector('#registerPassword')

// Register Form Submission
registerForm.addEventListener('submit', async (e) => {
  e.preventDefault()
  const username = userName.value
  const email = userEmail.value
  const pw = password.value
  const formObject = { username, email, pw }
  const res = await fetch('/register', {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json',
    },
    body: JSON.stringify(formObject),
  })
  registerForm.reset()
  if (res.status === 200) {
    window.location = '/'
  } else {
    res.status === 400
    const json = await res.json()
    alert(json.message)
  }
})
