import { Knex } from "knex";
import { TemplateInfo } from "../models/templateModel";

export class TemplateService {
  constructor(private knex: Knex) {}

  hideCv = async (userId: number) => {
    await this.knex("user_cv_details").update("is_active", "false").where("users_id", userId);
    return;
  };

  buildCv = async (templateInfo: TemplateInfo, userId: number, image: string) => {
    try {
      // console.log(users_id);

      console.log(templateInfo);
      const result = await this.knex("user_cv_details")
        .insert({
          image: image,
          firstname: templateInfo.firstName,
          lastname: templateInfo.lastName,
          address: templateInfo.address,
          description: templateInfo.description,
          phone_number: templateInfo.phoneNumber,
          email: templateInfo.email,
          users_id: userId,
          is_active: true,
        })
        .returning("id");

      const cvId = parseInt(result[0]);

      if (typeof templateInfo.jobTitle == "string") {
        await this.knex("work_exp").insert({
          job_title: templateInfo.jobTitle,
          company_name: templateInfo.companyName,
          start_year: templateInfo.companyStartYear,
          end_year: templateInfo.companyEndYear,
          user_cv_details_id: cvId,
        });
      } else {
        for (let i = 0; i < templateInfo.jobTitle.length; i++) {
          await this.knex("work_exp").insert({
            job_title: templateInfo.jobTitle[i],
            company_name: templateInfo.companyName[i],
            start_year: templateInfo.companyStartYear[i],
            end_year: templateInfo.companyEndYear[i],
            user_cv_details_id: cvId,
          });
        }
      }
      if (typeof templateInfo.schoolName == "string") {
        await this.knex("education").insert({
          school_name: templateInfo.schoolName,
          start_year: templateInfo.schoolStartYear,
          end_year: templateInfo.schoolEndYear,
          program_name: templateInfo.programName,
          user_cv_details_id: cvId,
        });
      } else {
        for (let i = 0; i < templateInfo.schoolName.length; i++) {
          await this.knex("education").insert({
            school_name: templateInfo.schoolName[i],
            start_year: templateInfo.schoolStartYear[i],
            end_year: templateInfo.schoolEndYear[i],
            program_name: templateInfo.programName[i],
            user_cv_details_id: cvId,
          });
        }
      }

      if (templateInfo.skills) {
        for (let skill of templateInfo.skills) {
          await this.knex("user_cv_skills").insert({
            skillset_id: skill,
            user_cv_details_id: cvId,
          });
        }
      }
    } catch (error) {
      throw new Error(`fail to insert into user_cv_details, \n ${error}`);
    }
  };

  getInfo = async (id: string) => {
    let CVdata = [];
    try {
      console.log("run the SQL");
      console.log({ user: id });
      CVdata = await this.knex
        .select(
          "users.id",
          "user_cv_details.id as user_cv_details_id",
          "user_cv_details.image",
          "user_cv_details.firstname",
          "user_cv_details.lastname",
          "user_cv_details.description",
          "user_cv_details.address",
          "user_cv_details.phone_number",
          "user_cv_details.email",
          "user_cv_details.is_active",
          this.knex.raw(
            `JSON_AGG( 
              json_build_object(
                'work', (SELECT array_AGG(work_exp.*) FROM work_exp WHERE work_exp.user_cv_details_id = user_cv_details.id),
                
                'education', (SELECT array_AGG(education.*) FROM education WHERE education.user_cv_details_id = user_cv_details.id),

                'user_cv_skills', (SELECT array_AGG(user_cv_skills.skillset_id) FROM user_cv_skills WHERE user_cv_skills.user_cv_details_id = user_cv_details.id ) 
              )
            ) AS all_exp`
          )
        )
        .from("user_cv_details")
        .leftJoin("user_cv_skills", "user_cv_skills.user_cv_details_id", "user_cv_details.id")
        .rightJoin("users", "users.id", "user_cv_details.users_id")
        .leftJoin("education", "user_cv_details.id", "education.user_cv_details_id")
        .leftJoin("work_exp", "user_cv_details.id", "work_exp.user_cv_details_id")
        .groupBy(
          "users.id",
          "user_cv_details.id",
          "user_cv_details.image",
          "user_cv_details.firstname",
          "user_cv_details.lastname",
          "user_cv_details.description",
          "user_cv_details.address",
          "user_cv_details.phone_number",
          "user_cv_details.email",
          "user_cv_details.is_active",
          "education.id",
          "work_exp.id",
          "user_cv_skills.id"
        )
        .where("users.id", id)
        .andWhere("user_cv_details.is_active", true)
        .first();
    } catch (error) {
      console.log(error.message);
      return;
    }

    // dev cm

    const skills = [];
    try {
      if (CVdata.user_cv_skills == "") {
        return CVdata;
      }
      for (let skill of CVdata.all_exp[0].user_cv_skills) {
        const result = await this.knex.select("skill_name").from("skillset").where("id", skill).first();
        console.log(result);
        skills.push(result.skill_name);
      }
      CVdata.all_exp[0].user_cv_skills = skills;
    } catch (error) {
      console.log("skill is N/A");
    }

    return CVdata;
  };

  getSkillSet = async () => {
    const skillSet = await this.knex.select("id", "skill_name").from("skillset");
    return skillSet;
  };

  mockquestion = async () => {
    const question = await this.knex.select("*").from("mock_questions").orderBy("id");
    return question;
  };

  getmockquestion = async (id: number) => {
    const question = await this.knex.select("*").from("mock_questions").where("id", id);
    return question;
  };

  userInfoDemo = async (userId: number) => {
    console.log({ userId });
    const userInfo = await this.knex
      .select(
        "users.username",
        "users.useremail",
        "users.password",
        // "user_cv_details.firstname",
        // "user_cv_details.lastname",
        // "user_cv_details.address",
        // "user_cv_details.phone_number",
        // "user_cv_details.email",
        // "user_cv_details.image",
        this.knex.raw(
          'ARRAY_AGG("user_cv_details.image","user_cv_details.firstname","user_cv_details.lastname","user_cv_details.address","user_cv_details.phone_number","user_cv_details.email")'
        )
        // "user_cv_details.is_active",
        // "work_exp.job_title",
        // "work_exp.company_name",
        // "work_exp.start_year as company_start_year",
        // "work_exp.end_year as company_end_year",
        // "education.school_name",
        // "education.start_year as school_name_start_year",
        // "education.end_year as school_name_end_year",
        // "education.program_name",
        // this.knex.raw(`array_agg('skillset.skill_name') as skillset`)
      )
      .from("user_cv_details")
      .leftJoin("users", "users.id", "user_cv_details.users_id")
      // .join("work_exp", "work_exp.user_cv_details_id", "user_cv_details.id")
      // .join("education", "education.user_cv_details_id", "user_cv_details.id")
      // .join("skillset", "skill.user_cv_details_id", "user_cv_details.id")
      .groupBy("users.username", "users.useremail", "users.password")
      .where("users.id", userId);
    // .first();
    return userInfo;
  };
}
