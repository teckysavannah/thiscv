import { Knex } from "knex";
import { hashPassword, checkPassword } from "../hash";

export class UserService {
  constructor(private knex: Knex) {}

  // Login for existing users
  login = async (email: string, password: string) => {
    console.log(email);
    if (!email || !password) {
      throw new Error("Missing email/password");
    }
    const loginResult = await this.knex("users").where("useremail", email).first();
    if (!loginResult) {
      throw new Error("Invalid username/password");
    }
    const isValidPassword = await checkPassword(password, loginResult.password);
    if (!isValidPassword) {
      throw new Error("Invalid username/password");
    }
    return loginResult;

    // const { id, username } = user
    // req.session["user"] = { id, username, email };
    // res.status(200).end()
  };

  //Register for new user
  register = async (username: string, email: string, password: string) => {
    const hashedPassword = await hashPassword(password);
    const userResult = await this.knex.count("useremail").from("users").where("useremail", email);
    // console.log(userResult);
    if (userResult[0].count >= 1) {
      throw new Error("Email has already been registered");
    }
    const id = await this.knex
      .insert({ username: username, useremail: email, password: hashedPassword })
      .into("users")
      .returning("id");
    return id;
  };

  loginGoogle = async (email: string) => {
    try {
      // console.log("select user from SQL");
      const user = await this.knex.select("*").from("users").where("username", email).first();
      return user;
    } catch (error) {
      throw new Error("fail to select user @loginGoogle");
    }
  };
  createGoogleUser = async (email: string, username: string) => {
    const googleUser = await this.knex
      .insert({ useremail: email, password: "", username })
      .into("users")
      .returning("id");
    return { id: googleUser };
  };
}
