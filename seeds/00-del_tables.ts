import { Knex } from "knex";
import { tables } from "../utils/tables";

export async function seed(knex: Knex): Promise<void> {
  //   const trx = await knex.transaction();
  //   try {
  await knex.raw(`TRUNCATE ${tables.USER_TEMPLATE} RESTART IDENTITY CASCADE`);
  await knex.raw(`TRUNCATE ${tables.OUR_SAMPLE} RESTART IDENTITY CASCADE`);
  await knex.raw(`TRUNCATE ${tables.USERS_MOCK_VIDEO} RESTART IDENTITY CASCADE`);
  await knex.raw(`TRUNCATE ${tables.MOCK_QUESTTIONS} RESTART IDENTITY CASCADE`);
  await knex.raw(`TRUNCATE ${tables.USER_CV_SKILLS} RESTART IDENTITY CASCADE`);
  await knex.raw(`TRUNCATE ${tables.SKILLSET} RESTART IDENTITY CASCADE`);
  await knex.raw(`TRUNCATE ${tables.WORK_EXP} RESTART IDENTITY CASCADE`);
  await knex.raw(`TRUNCATE ${tables.EDUCATION} RESTART IDENTITY CASCADE`);
  await knex.raw(`TRUNCATE ${tables.USER_CV_DETAILS} RESTART IDENTITY CASCADE`);
  await knex.raw(`TRUNCATE ${tables.USER} RESTART IDENTITY CASCADE`);
  //   } catch (e) {
  //     console.log(e);
  //   }
}
