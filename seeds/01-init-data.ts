import { Knex } from "knex";
import { hashPassword } from "../hash";
import { tables } from "../utils/tables";

const users = [
  { username: "Jason", useremail: "jason@tecky.io", password: "1234" },
  { username: "Alex", useremail: "alex@tecky.io", password: "1234" },
  { username: "Gordon", useremail: "gordon@tecky.io", password: "1234" },
];

const user_cv_details = [
  {
    image: "jason.jpeg",
    firstname: "Jason",
    lastname: "Li",
    address: "Hong Kong",
    phone_number: "61779987",
    description:
      " I am a web front-end developer, graduated from Hong Kong and my major is CS. I recently try my best to improve my skills on web front-end. My favorite thing is to observe others' portfolio.",
    email: "jason@tecky.io",
    is_active: "true",
    users_id: 1,
  },
  {
    image: "alex.jpeg",
    firstname: "Alex",
    lastname: "Lau",
    address: "Tsuen Wan West",
    phone_number: "95863335",
    description:
      "Graduate of computer science with experience working across the full-stack of software development. I have built 30+ projects on 7 teams.",
    email: "alex@tecky.io",
    is_active: "true",
    users_id: 2,
  },
  {
    image: "gordonlau.jpeg",
    firstname: "Gordon",
    lastname: "Lau",
    address: "New York, USA",
    phone_number: "67999055",
    description:
      "Python developer with extensive experience working with Big Data. I am equally comfortable working solo as I am in a group setting.",
    email: "alex@tecky.io",
    is_active: "true",
    users_id: 3,
  },
];

const education = [
  {
    school_name: "tecky small school",
    start_year: "1990",
    end_year: "1992",
    program_name: "art",
    user_cv_details_id: 1,
  },
  {
    school_name: "tecky big school",
    start_year: "2000",
    end_year: "2003",
    program_name: "maths",
    user_cv_details_id: 2,
  },
  { school_name: "tecky school", start_year: "2011", end_year: "2014", program_name: "english", user_cv_details_id: 3 },
];

const work_exp = [
  { job_title: "Business", company_name: "ocean park", start_year: "2001", end_year: "2003", user_cv_details_id: 1 },
  { job_title: "Engineering", company_name: "car park", start_year: "2008", end_year: "2010", user_cv_details_id: 2 },
  { job_title: "Hospitality", company_name: "john park", start_year: "2019", end_year: "2020", user_cv_details_id: 3 },
];

// const skillset = [
//   { skill_name: "[js, css, html]", user_cv_details_id: 1 },
//   { skill_name: "[js, css, html, python]", user_cv_details_id: 2 },
//   { skill_name: "[js, css, html, python, SQL]", user_cv_details_id: 3 },
// ];

const skillset = [
  { skill_name: "Angular" },
  { skill_name: "C++" },
  { skill_name: "Java" },
  { skill_name: "JavaScript" },
  { skill_name: "PHP" },
  { skill_name: "Python" },
  { skill_name: "React" },
  { skill_name: "Typescript" },
  { skill_name: "Express" },
  { skill_name: "Git" },
  { skill_name: "Knex" },
  { skill_name: "Node" },
  { skill_name: "Mongo DB" },
  { skill_name: "PostgreSQL" },
  { skill_name: "Tensorflow" },
];

// const user_cv_skills = [
//   { user_cv_details_id: 1, skillset_id: 1 },
//   { user_cv_details_id: 1, skillset_id: 2 },
//   { user_cv_details_id: 1, skillset_id: 3 },
//   { user_cv_details_id: 1, skillset_id: 9 },
//   { user_cv_details_id: 1, skillset_id: 10 },
//   { user_cv_details_id: 2, skillset_id: 3 },
//   { user_cv_details_id: 2, skillset_id: 6 },
//   { user_cv_details_id: 2, skillset_id: 13 },
//   { user_cv_details_id: 2, skillset_id: 14 },
//   { user_cv_details_id: 3, skillset_id: 2 },
//   { user_cv_details_id: 3, skillset_id: 3 },
//   { user_cv_details_id: 3, skillset_id: 4 },
//   { user_cv_details_id: 3, skillset_id: 6 },
//   { user_cv_details_id: 3, skillset_id: 8 },
//   { user_cv_details_id: 3, skillset_id: 9 },
//   { user_cv_details_id: 1, skillset_id: 1 },
//   { user_cv_details_id: 1, skillset_id: 2 },
//   { user_cv_details_id: 1, skillset_id: 3 },
//   { user_cv_details_id: 1, skillset_id: 1 },
//   { user_cv_details_id: 1, skillset_id: 2 },
//   { user_cv_details_id: 1, skillset_id: 3 },

// ];
const user_cv_skills:object[] = [];
const skillidSet = new Set();
let skillset_id;
function initSkillidSet() {
  skillidSet.clear();
  for (let i = 0; i < 15; i++) {
    skillidSet.add(i + 1);
  }
}

for (let i = 0; i < 3; i++) {
  initSkillidSet();
  for (let j = 0; j < 5; j++) {
    do {
      skillset_id = Math.ceil(Math.random() * 15);
    } while (!skillidSet.has(skillset_id));
    user_cv_skills.push({ user_cv_details_id: i + 1, skillset_id });
    skillidSet.delete(skillset_id);
  }
}

const mock_questions = [
  {
    question:
      "Why do we need to install Node on our machines to run JavaScript file when we can just run them inside browser?",
  },
  { question: "What is 'callback hell'? How do I fix callback hell?" },
  { question: "What do you do first when creating something new?" },
  { question: "What is Computer programming?" },
  { question: "When a syntax error occurs?" },
  { question: "Why Mocking is necessary in Automatic Testing?" },
  { question: "When do we need Indexing?" },
  { question: "Why Nginx is used as a reverse proxy in front of the NodeJS application?" },
  { question: "Why did you want to become a programmer?" },
  { question: "Tell me about some projects you've completed. How you work with your group-mates?" },
];

// const users_mock_video = [
//   { file_name: "123.mp4", users_id: 1, mock_questions_id: 2 },
//   { file_name: "123.mp4", users_id: 2, mock_questions_id: 3 },
//   { file_name: "123.mp4", users_id: 3, mock_questions_id: 1 },
// ];

// const our_sample = [
//   { sample_name: "123.mp4", price: "1000", file_name: "1234" },
//   { sample_name: "123.mp4", price: "2000", file_name: "2345" },
//   { sample_name: "123.mp4", price: "3000", file_name: "3456" },
// ];

// const user_template = [{ users_id: 1 }, { users_id: 2 }, { users_id: 3 }];

export async function seed(knex: Knex): Promise<void> {
  // const trx = await knex.transaction();
  // try {
  // const users = await Promise.all(
  //   userData.map(async (user) => ({
  //     ...user,
  //     password: await hashPassword(user.password.toString()),
  //   }))
  // );
  // await trx("users").insert(users);

  for (const user of users) {
    user.password = await hashPassword(user.password);
  }

  await knex(tables.USER).insert(users);
  await knex(tables.USER_CV_DETAILS).insert(user_cv_details);
  await knex(tables.EDUCATION).insert(education);
  await knex(tables.WORK_EXP).insert(work_exp);
  await knex(tables.SKILLSET).insert(skillset);
  await knex(tables.USER_CV_SKILLS).insert(user_cv_skills);
  await knex(tables.MOCK_QUESTTIONS).insert(mock_questions);
  // await knex(tables.USERS_MOCK_VIDEO).insert(users_mock_video);
  // await knex(tables.OUR_SAMPLE).insert(our_sample);
  // await knex(tables.USER_TEMPLATE).insert(user_template);
  // } catch (e) {
  //   console.log(e);
  // }
}
