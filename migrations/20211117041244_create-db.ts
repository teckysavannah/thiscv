import { Knex } from "knex";

export async function up(knex: Knex): Promise<void> {
  await knex.schema.createTable("users", (table) => {
    table.increments();
    table.string("username");
    table.string("useremail");
    table.string("password");
    table.timestamps(false, true);
  });

  await knex.schema.createTable("user_cv_details", (table) => {
    table.increments();
    table.string("image");
    table.string("description");
    table.string("firstname");
    table.string("lastname");
    table.text("address");
    table.string("phone_number");
    table.string("email");
    table.boolean("is_active");
    table.timestamps(false, true);
    table.integer("users_id");
    table.foreign("users_id").references("users.id");
  });

  await knex.schema.createTable("education", (table) => {
    table.increments();
    table.string("school_name");
    table.string("start_year");
    table.string("end_year");
    table.string("program_name");
    table.timestamps(false, true);
    table.integer("user_cv_details_id");
    table.foreign("user_cv_details_id").references("user_cv_details.id");
  });

  await knex.schema.createTable("work_exp", (table) => {
    table.increments();
    table.string("job_title");
    table.text("company_name");
    table.string("start_year");
    table.string("end_year");
    table.timestamps(false, true);
    table.integer("user_cv_details_id");
    table.foreign("user_cv_details_id").references("user_cv_details.id");
  });

  await knex.schema.createTable("skillset", (table) => {
    table.increments();
    table.string("skill_name");
    table.timestamps(false, true);
  });

  await knex.schema.createTable("user_cv_skills", (table) => {
    table.increments();
    table.timestamps(false, true);
    table.integer("user_cv_details_id");
    table.foreign("user_cv_details_id").references("user_cv_details.id");
    table.integer("skillset_id");
    table.foreign("skillset_id").references("skillset.id");
  });

  await knex.schema.createTable("mock_questions", (table) => {
    table.increments();
    table.string("question");
    table.timestamps(false, true);
  });

  await knex.schema.createTable("users_mock_video", (table) => {
    table.increments();
    table.string("file_name");
    table.timestamps(false, true);
    table.integer("users_id");
    table.foreign("users_id").references("users.id");
    table.integer("mock_questions_id");
    table.foreign("mock_questions_id").references("mock_questions.id");
  });

  await knex.schema.createTable("our_sample", (table) => {
    table.increments();
    table.string("sample_name");
    table.integer("price");
    table.string("file_name");
    table.timestamps(false, true);
  });

  await knex.schema.createTable("user_template", (table) => {
    table.increments();
    table.timestamps(false, true);
    table.integer("users_id");
    table.foreign("users_id").references("users.id");
  });
}

export async function down(knex: Knex): Promise<void> {
  await knex.schema.dropTable("user_template");
  await knex.schema.dropTable("our_sample");
  await knex.schema.dropTable("users_mock_video");
  await knex.schema.dropTable("mock_questions");
  await knex.schema.dropTable("user_cv_skills");
  await knex.schema.dropTable("skillset");
  await knex.schema.dropTable("work_exp");
  await knex.schema.dropTable("education");
  await knex.schema.dropTable("user_cv_details");
  await knex.schema.dropTable("users");
}
