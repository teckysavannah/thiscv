import { TemplateService } from "../services/templateService";
import { Request, Response } from "express";
import { TemplateInfo } from "../models/templateModel";

export class TemplateController {
  constructor(private templateService: TemplateService) {}
  buildCv = async (req: Request, res: Response) => {
    try {
      const templateInfo: TemplateInfo = req.body;
      const image = req.file?.filename || "";
      console.log(req.file);
      const userId = req.session["user"].id;
      await this.templateService.hideCv(userId);
      await this.templateService.buildCv(templateInfo, userId, image);
      res.status(200).json({ message: "success" });
      return;
    } catch (error) {
      res.status(404).json(error.toString());
      return;
    }
  };

  getInfo = async (req: Request, res: Response) => {
    console.log(req.session["user"]);
    const { id } = req.session["user"];
    // const id=req.session.id
    const users = await this.templateService.getInfo(id);
    if (!users) {
      res.status(400).json({ message: "No CV is available" });
      return;
    }
    res.json({ users });
    return;
  };

  userInfoDemo = async (req: Request, res: Response) => {
    const userId = req.body.userId;
    const userInfo = await this.templateService.userInfoDemo(userId);
    res.status(200).json({ userInfo });
  };
  getSkillSet = async (req: Request, res: Response) => {
    const skillSet: string[] = await this.templateService.getSkillSet();
    res.json(skillSet);
  };

  mockquestion = async (req: Request, res: Response) => {
    if (req.params.id) {
      const data = await this.templateService.getmockquestion(parseInt(req.params.id));
      res.json(data);
      console.log(data);
      return;
    }
    const question = await this.templateService.mockquestion();
    res.json(question);
    // console.log(req.params.id);
  };

  // {skillSet:['JS',"TS",,,]}

  // catch(err){
  //     res.status(400).json({ message: err.message });
}
