import { UserService } from "../services/userService";
import { Request, Response } from "express";
import fetch from "node-fetch";

export class UserController {
  constructor(private userService: UserService) {}
  register = async (req: Request, res: Response) => {
    try {
      const { username, email, pw } = req.body;
      const result = await this.userService.register(username, email, pw);
      res.status(200).json({ result });
    } catch (err) {
      res.status(400).json({ message: err.message });
    }
  };
  loadCurrentUser = async (req: Request, res: Response) => {
    try {
      console.log(`loadCurrentUser : ` + req.session["user"]);

      if (!req.session["user"]) {
        console.log("session has no user");
        res.status(400).json("please login first");
        return;
      }
      const user = req.session["user"];
      res.status(200).json(user);
    } catch (error) {
      res.status(400).json("fail to check session");
    }
  };
  login = async (req: Request, res: Response) => {
    try {
      const { email, password } = req.body;
      const result = await this.userService.login(email, password);
      // console.log(result);
      req.session["user"] = { userName: result.username, id: result.id };
      console.log(`login : ` + req.session["user"]);

      res.status(200).json({ result: req.session["user"].id });
    } catch (err) {
      res.status(400).json({ message: err.message });
    }
  };

  logout = async (req: Request, res: Response) => {
    if (req.session["user"]) {
      delete req.session["user"];
    }
    // res.json({ success: true });
    res.redirect("/");
  };

  loginGoogle = async (req: Request, res: Response) => {
    const accessToken = req.session?.["grant"].response.access_token;
    console.log(accessToken);
    console.log("fetch get to Google");
    const fetchRes = await fetch("https://www.googleapis.com/oauth2/v2/userinfo", {
      method: "get",
      headers: {
        Authorization: `Bearer ${accessToken}`,
      },
    });
    // console.log(fetchRes);
    const result = await fetchRes.json();
    // console.log(result);
    let user = await this.userService.loginGoogle(result.email);

    if (!user) {
      // return res.status(401).redirect("/login.html?error=Incorrect+Username");
      user = await this.userService.createGoogleUser(result.email, result.name);
    }
    if (req.session) {
      req.session["user"] = {
        userName: result.name,
        id: user.id,
      };
    }
    return res.redirect("/");
  };
}
